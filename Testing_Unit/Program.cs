﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IM_Course_Project;
using System.Collections;
using System.Threading;

namespace Testing_Unit
{
    public struct Data
    {
        int steps, experiments, precision, iterations;
        double meanNoisy, meanNormal;

        public int Steps { get => steps; set => steps = value; }
        public int Experiments { get => experiments; set => experiments = value; }
        public int Precision { get => precision; set => precision = value; }
        public int Iterations { get => iterations; set => iterations = value; }
        public double MeanNoisy { get => meanNoisy; set => meanNoisy = value; }
        public double MeanNormal { get => meanNormal; set => meanNormal = value; }

        public Data(int s, int e, int p, int i)
        {
            steps = s;
            experiments = e;
            precision = p;
            iterations = i;
            meanNoisy = 0;
            meanNormal = 0;
        }
    }

    class Program
    {
        static Model m = new Model();

        static int Iterations = 50;

        static int[] Steps = { 20, 50, 100 };
        static int[] Experiments = { 200, 500, 1000 };
        static int[] Precisions = { 20, 50, 100 };

        List<Data> data = new List<Data>();

        static void Main(string[] args)
        {
            List<Data> data = new List<Data>();

            foreach (int step in Steps)
            {
                Data temp = new Data(step, Experiments[1], Precisions[1], Iterations);

                Parallel.For(0, Iterations, i => {
                    m.generateNewSurfaces(step, Experiments[1], Precisions[1], false);
                    temp.MeanNoisy += m.Cns();
                    temp.MeanNormal += m.Cnm();
                    Console.Write('.');
                });
                temp.MeanNoisy /= Iterations;
                temp.MeanNormal /= Iterations;
                data.Add(temp);
                Console.WriteLine("{0} steps experiment is done.", step);
            }

            foreach (int experiment in Experiments)
            {
                Data temp = new Data(Steps[1], experiment, Precisions[1], Iterations);

                Parallel.For(0, Iterations, i =>
                {
                    m.generateNewSurfaces(Steps[1], experiment, Precisions[1], false);
                    temp.MeanNoisy += m.Cns();
                    temp.MeanNormal += m.Cnm();
                    Console.Write('.');
                });
                temp.MeanNoisy /= Iterations;
                temp.MeanNormal /= Iterations;
                data.Add(temp);
                Console.WriteLine("{0} experiments experiment is done.", experiment);
            }

            foreach(int precision in Precisions)
            {
                Data temp = new Data(Steps[1], Experiments[1], precision, Iterations);

                Parallel.For(0, Iterations, i =>
                {
                    m.generateNewSurfaces(Steps[1], Experiments[1], precision, false);
                    temp.MeanNoisy += m.Cns();
                    temp.MeanNormal += m.Cnm();
                    Console.Write('.');
                });
                temp.MeanNoisy /= Iterations;
                temp.MeanNormal /= Iterations;
                data.Add(temp);
                Console.WriteLine("{0} precision experiment is done.", precision);
            }

            foreach(Data d in data)
            {
                Console.WriteLine("Steps = {0}; Experiments = {1}; Precision = {2}; Mean Noisy deviation = {3}; Mean Normalized deviaion = {4}",d.Steps,d.Experiments,d.Precision,d.MeanNoisy,d.MeanNormal);
            }

            do
            {
                while (!Console.KeyAvailable)
                {

                }
            }
            while (Console.ReadKey(true).Key != ConsoleKey.Escape);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IM_Course_Project
{
    class Regression
    {
        List<List<double>> designMatrix = new List<List<double>>();
        List<double> responseVector = new List<double>();
        List<double> parameterVector = new List<double>();
        List<double> residualsVector = new List<double>();

        List<List<double>> transposedMatrix = new List<List<double>>();
        List<List<double>> invertedMatrix = new List<List<double>>();

        public List<List<double>> DesignMatrix { get => designMatrix; set => designMatrix = value; }
        public List<double> ResponseVector { get => responseVector; set => responseVector = value; }
        public List<double> ParameterVector { get => parameterVector; set => parameterVector = value; }
        public List<double> ResidualsVector { get => residualsVector; set => residualsVector = value; }
        public List<List<double>> TransposedMatrix { get => transposedMatrix; set => transposedMatrix = value; }
        public List<List<double>> InvertedMatrix { get => invertedMatrix; set => invertedMatrix = value; }

        public Regression(List<List<double>> X, List<double> Y)
        {
            DesignMatrix = X;
            responseVector = Y;

            
            TransposedMatrix = Transpose(DesignMatrix);
            List<List<double>> XtX = Multiply(TransposedMatrix, DesignMatrix);
            List<double> XtY = Multiply(TransposedMatrix, ResponseVector);
            List<List<double>> XR = Invert(XtX);
            ParameterVector = Multiply(XR, XtY);
            

            /*
            TransposedMatrix = Transpose(DesignMatrix);
            List<List<double>> XtX = Multiply(TransposedMatrix, DesignMatrix);
            List<List<double>> XR = Invert(XtX);
            List<List<double>> invXtX = Multiply(XR, TransposedMatrix);
            List<double> XtY = Multiply(invXtX, ResponseVector);
            ParameterVector = Multiply(XR, XtY);
            */
        }

        public Regression() { }

        private List<List<double>> Invert(List<List<double>> matrix)
        {
            List<List<double>> inv = new List<List<double>>();

            //Посчитать определитель D, D != 0
            double D = Determinant(matrix);

            //Составить алгебраические дополнения в отдельную матрицу
            int res;
            int[] perm;

            List<List<double>> add = MatrixDecompose(matrix, out perm, out res);
            if (add == null) throw new Exception("Could not compute determinant.");

            //Транспонировать матрицу дополнений
            inv = Transpose(add);

            //Поделить все элементы матрицы дополнений на определитель
            foreach(List<double> row in inv)
            {
                for (int i = 0; i < row.Count; i++)
                {
                    row[i] /= D;
                }
            }


            return inv;
        }

        public List<List<double>> Design(List<List<double>> x)
        {
            List<List<double>> des = new List<List<double>>();

            for (int i = 0; i < x.Count; i++)
            {
                List<double> temp = new List<double>();
                temp.Add(1.0);
                temp.AddRange(x[i]);
                des.Add(temp);
            }

            return des;
        }

        private List<List<double>> Multiply(List<List<double>> leftMatrix, List<List<double>> rightMatrix)
        {
            if (leftMatrix[0].Count != rightMatrix.Count) throw new Exception("Could not compute m x m");

            List<List<double>> mult = new List<List<double>>();
            for(int i = 0; i < leftMatrix.Count; i++)
            {
                List<double> temp = new List<double>();
                for(int t = 0; t < leftMatrix.Count; t++) { temp.Add(0); }
                mult.Add(temp);
            }

            for (int i = 0; i < leftMatrix.Count; i++)
            {
                for (int j = 0; j < rightMatrix[0].Count; j++)
                {
                    double temp = 0;
                    for (int k = 0; k < leftMatrix[0].Count; k++)
                    {
                        temp += leftMatrix[i][k] * rightMatrix[k][j];
                    }
                    mult[i][j] = temp;
                }
            }

            return mult;
        }

        private List<double> Multiply(List<List<double>> matrix, List<double> vector)
        {
            if (matrix[0].Count != vector.Count) throw new Exception("Could not compute m x v");

            List<double> mult = new List<double>();
            for (int i = 0; i < matrix.Count; i++)
            {
                mult.Add(0);
            }

            for (int i = 0; i < matrix.Count; i++)
            {
                for (int j = 0; j < vector.Count; j++)
                {
                    mult[i] += matrix[i][j] * vector[j];
                }
            }

            return mult;
        }

        public List<List<double>> Transpose(List<List<double>> designMatrix)
        {
            List<List<double>> transp = new List<List<double>>();

            for(int i = 0; i<designMatrix[0].Count;i++)
            {
                transp.Add(new List<double>());
            }

            for (int i = 0; i < designMatrix.Count; i++)
            {
                for (int j = 0; j < transp.Count; j++)
                {
                    transp[j].Add(designMatrix[i][j]);
                }
            }
            return transp;
        }
 

        //----------------------------------------------------------------

        double Determinant(List<List<double>> matrix)
        {
            double d = 0;
            int res;
            int[] perm;

            List<List<double>> temp = MatrixDecompose(matrix, out perm, out res);
            if (temp == null) throw new Exception("Could not compute determinant.");

            d = res;
            for (int i = 0; i < temp.Count; i++)
            {
                d *= temp[i][i];
            }

            return d;
        }

        static List<List<double>> MatrixDecompose(List<List<double>> matrix, out int[] perm,
      out int toggle)
        {
            // Doolittle LUP decomposition with partial pivoting.
            // returns: result is L (with 1s on diagonal) and U;
            // perm holds row permutations; toggle is +1 or -1 (even or odd)
            int rows = matrix.Count;
            int cols = matrix[0].Count;
            if (rows != cols)
                throw new Exception("Non-square mattrix");

            int n = rows; // convenience

            List<List<double>> result = matrix; // 

            perm = new int[n]; // set up row permutation result
            for (int i = 0; i < n; ++i) { perm[i] = i; }

            toggle = 1; // toggle tracks row swaps

            for (int j = 0; j < n - 1; ++j) // each column
            {
                double colMax = Math.Abs(result[j][j]);
                int pRow = j;

                for (int i = j + 1; i < n; ++i) // reader Matt V needed this:
                {
                    if (Math.Abs(result[i][j]) > colMax)
                    {
                        colMax = Math.Abs(result[i][j]);
                        pRow = i;
                    }
                }
                // Not sure if this approach is needed always, or not.

                if (pRow != j) // if largest value not on pivot, swap rows
                {
                    List<double> rowPtr = result[pRow];
                    result[pRow] = result[j];
                    result[j] = rowPtr;

                    int tmp = perm[pRow]; // and swap perm info
                    perm[pRow] = perm[j];
                    perm[j] = tmp;

                    toggle = -toggle; // adjust the row-swap toggle
                }

                // -------------------------------------------------------------
                // This part added later (not in original code) 
                // and replaces the 'return null' below.
                // if there is a 0 on the diagonal, find a good row 
                // from i = j+1 down that doesn't have
                // a 0 in column j, and swap that good row with row j

                if (result[j][j] == 0.0)
                {
                    // find a good row to swap
                    int goodRow = -1;
                    for (int row = j + 1; row < n; ++row)
                    {
                        if (result[row][j] != 0.0)
                            goodRow = row;
                    }

                    if (goodRow == -1)
                        throw new Exception("Cannot use Doolittle's method");

                    // swap rows so 0.0 no longer on diagonal
                    List<double> rowPtr = result[goodRow];
                    result[goodRow] = result[j];
                    result[j] = rowPtr;

                    int tmp = perm[goodRow]; // and swap perm info
                    perm[goodRow] = perm[j];
                    perm[j] = tmp;

                    toggle = -toggle; // adjust the row-swap toggle
                }
                // -------------------------------------------------------------

                for (int i = j + 1; i < n; ++i)
                {
                    result[i][j] /= result[j][j];
                    for (int k = j + 1; k < n; ++k)
                    {
                        result[i][k] -= result[i][j] * result[j][k];
                    }
                }

            } // main j column loop

            return result;
        } // MatrixDecompose

        // -------------------------------------------------------------
    }
}

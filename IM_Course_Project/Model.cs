﻿using nzy3D.Colors;
using nzy3D.Colors.ColorMaps;
using nzy3D.Maths;
using nzy3D.Plot3D.Builder;
using nzy3D.Plot3D.Builder.Concrete;
using nzy3D.Plot3D.Primitives;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace IM_Course_Project
{
    //Статический класс со всякими штуками
    public static class fe
    {
        //Факторное пространство
        public static Range rangex1 = new Range(0.8, 1.3);
        public static Range rangex2 = new Range(0.4, 1);

        public static double x0 = 0;

        //Начальные параметры шага, эксперимента и точности нормального распределения
        public static int steps = 30;
        public static int Experiments = 300;
        public static int iter = 50;

        //Формула динамической модели
        public static double f(double x1, double x2)
        {
            return (x1 - x2) * Math.Log(x1 * x2);
        }

        //Расчет значения нормального распределения от случайной равномерно распределенной величины методом прямоугольников
        public static double normalIntX(double x, double s, double x0, int iter)
        {
            double tempx = 0;
            double dx = (20 * s / iter);

            double fx = 0;
            double prevx = 0;
            tempx = x0 - 10 * s;
            while (fx <= x)
            {
                double height = GetFunctionResult(tempx, s, x0);
                prevx = tempx;
                fx += height * dx;
                tempx += dx;
            }

            tempx = (tempx + prevx) / 2;

            return tempx;
        }

        //Расчет значения функции нормального распределения
        public static double GetFunctionResult(double x, double s, double x0)
        {
            double fx = 0;
            double exppow = 0;

            exppow = -(Math.Pow(x - x0, 2) / (2 * Math.Pow(s, 2)));
            fx = Math.Exp(exppow) / (Math.Sqrt(2 * Math.PI) * s);

            return fx;
        }

        //Расчет среднеквадратичного отклонения
        public static double Sigma(Range rangex, Range rangey)
        {
            return 0.15 * fe.f((rangex.Max + rangex.Min) / 2, (rangey.Max + rangey.Min) / 2); ;
        }

        public static void WriteInFile(List<double> Y, List<double> x1, List<double> x2)
        {
            var sw = new StreamWriter("text.csv");

            sw.WriteLine( "Y;X1;X2;X1X2;X1X1;X2X2");
            for (int i = 0; i < Y.Count; i++)
            {
                sw.WriteLine(Y[i] + ";" + x1[i] + ";" + x2[i] + ";" + x1[i] * x2[i] + ";" + x1[i] * x1[i] + ";" + x2[i] * x2[i]);
            }
            sw.Close();

        }
    }

    //Маленький класс-счетчик для подсчета среднего отклонения (шума) в экспериментальных моделях
    public class Counter
    {
        public int counter = 0;
        public double sumNoise = 0;

        public Counter() { }

        public double meanNoise()
        {
            return sumNoise / counter;
        }
    }

    public class Multipliers
    {
        public double b0, b1, b2, b3, b4, b5;

        public Multipliers() { }
    }

    public class Model
    {
        private Shape perfectSurface, noisySurface, normalSurface, regressionSurface;
        private double cns, cnm, crg;
        public double b0, b1, b2, b3, b4, b5;
        public List<double> X1 = new List<double>();
        public List<double> X2 = new List<double>();
        public List<double> Y = new List<double>();

        public Shape PerfectSurface() { return perfectSurface; }
        public void PerfectSurface(Shape ps) { perfectSurface = ps; }
        public Shape NoisySurface() { return noisySurface; }
        public void NoisySurface(Shape ps) { noisySurface = ps; }
        public Shape NormalSurface() {return normalSurface;}
        public void NormalSurface(Shape ps) {normalSurface = ps;}
        public Shape RegressionSurface() { return regressionSurface; }
        public void RegressionSurface(Shape ps) { regressionSurface = ps; }
        public double Cns() { return cns; }
        public void Cns(double ps) { cns = ps; }
        public double Cnm() { return cnm; }
        public void Cnm(double ps) { cnm = ps; }
        public double Crg() { return crg; }
        public void Crg(double ps) { crg = ps; }

        public Model()
        {
            //Создаем три поверхности-модели
            noisyMapper nsm = new noisyMapper();
            normalMapper nrm = new normalMapper();
            regressionMapper rsm = new regressionMapper();

            perfectSurface = newSurface(new perfectMapper(), true);
            noisySurface = newSurface(nsm, true);
            normalSurface = newSurface(nrm, true);
            regressionSurface = newSurface(rsm, true);
            
            cns = nsm.C().meanNoise();
            cnm = nrm.C().meanNoise();
            crg = rsm.C().meanNoise();

            b0 = rsm.ML().b0;
            b1 = rsm.ML().b1;
            b2 = rsm.ML().b2;
            b3 = rsm.ML().b3;
            b4 = rsm.ML().b4;
            b5 = rsm.ML().b5;

            X1 = rsm.X1;
            X2 = rsm.X2;
            Y = rsm.Y;

            fe.WriteInFile(Y, X1, X2);
        }

        //Генерация новых экспериментальных поверхностей-моделей с заданными параметрами шага, экспериментов и точности нормального распределения
        public void generateNewSurfaces(int steps, int experiments, int noiseacc, bool wd)
        {
            fe.steps = steps;
            fe.Experiments = experiments;
            fe.iter = noiseacc;

            noisyMapper nsm = new noisyMapper();
            normalMapper nrm = new normalMapper();
            regressionMapper rsm = new regressionMapper();

            perfectSurface = newSurface(new perfectMapper(), wd);
            noisySurface = newSurface(nsm, wd);
            normalSurface = newSurface(nrm, wd);
            regressionSurface = newSurface(rsm, wd);

            cns = nsm.C().meanNoise();
            cnm = nrm.C().meanNoise();
            crg = rsm.C().meanNoise();

            b0 = rsm.ML().b0;
            b1 = rsm.ML().b1;
            b2 = rsm.ML().b2;
            b3 = rsm.ML().b3;
            b4 = rsm.ML().b4;
            b5 = rsm.ML().b5;

            X1 = rsm.X1;
            X2 = rsm.X2;
            Y = rsm.Y;

            fe.WriteInFile(Y, X1, X2);
        }

        //Метод для генерации одной новой поверхности
        public Shape newSurface(nzy3D.Plot3D.Builder.Mapper mapper, bool WireframeDisplayed)
        {
            Shape Surface = Builder.buildOrthonomal(new OrthonormalGrid(fe.rangex1, fe.steps, fe.rangex2, fe.steps), mapper);
            return reshape(Surface, WireframeDisplayed);
        }

        //Метод для придания поверхности цвета и сетки
        private Shape reshape(Shape surface, bool wd)
        {
            surface.ColorMapper = new ColorMapper(new ColorMapRainbow(), surface.Bounds.zmin, surface.Bounds.zmax, new Color(1.0, 1.0, 1.0, 1.0));
            surface.FaceDisplayed = true;
            surface.WireframeDisplayed = wd;
            surface.WireframeColor = new Color(0, 1.0, 1.0, 0.7);

            return surface;
        }
    }

    //В этом классе высчитывается математическая модель
    class perfectMapper : nzy3D.Plot3D.Builder.Mapper
    {
        //Переопределение функции класса для отрисовки чистой поверхности
        public override double f(double x, double y)
        {
            //Просто считаем значения Y в точках X1,X2, пробегая по сетке с заданным шагом
            return fe.f(x, y);
        }
    }

    //В этом классе высчитывается «грязная» экспериментальная модель
    public class noisyMapper : nzy3D.Plot3D.Builder.Mapper
    {
        Random rng = new Random();
        double Sigma = fe.Sigma(fe.rangex1, fe.rangex2);

        Counter c = new Counter();
        public Counter C() { return c; }
        public void C(Counter ps) { c = ps; }

        //Переопределение функции класса для отрисовки грязной поверхности
        public override double f(double x, double y)
        {
            //Здесь считаем значения Y в точках X1,X2, пробегая по сетке с заданным шагом, но докидываем шум
            //Здесь важна точность шума, определяющая шаг в расчете нормальных значений для шума методом прямоугольников.
            double xn = rng.NextDouble();
            rng = new Random(rng.Next());
            c.counter++;
            double noise = fe.normalIntX(xn, Sigma, fe.x0, fe.iter);
            c.sumNoise += Math.Abs(noise);
            return fe.f(x, y) + noise;
        }
    }

    //В этом классе высчитывается аппроксимационная экспериментальная модель
    public class normalMapper : nzy3D.Plot3D.Builder.Mapper
    {
        Random masterrng, rng1, rng2, rngN;
        double Sigma = fe.Sigma(fe.rangex1, fe.rangex2);
        public List<double> X1, X2, Y;
        double stepx1 = fe.rangex1.Range / fe.steps;
        double stepx2 = fe.rangex2.Range / fe.steps;

        Counter c = new Counter();
        public Counter C() { return c; }
        public void C(Counter ps) { c = ps; }

        //При создании объекта класса генерируется набор случайных точек X1,X2 в заданном факторном пространстве
        public normalMapper()
        {
            masterrng = new Random();
            rng1 = new Random(masterrng.Next());
            rng2 = new Random(masterrng.Next());
            rngN = new Random(masterrng.Next());

            X1 = new List<double>();
            X2 = new List<double>();

            Range rangex1 = fe.rangex1;
            Range rangex2 = fe.rangex2;
            for (int i = 0; i < fe.Experiments; i++)
            {
                double x1 = rng1.NextDouble() * (rangex1.Max - rangex1.Min) + rangex1.Min;
                double x2 = rng2.NextDouble() * (rangex2.Max - rangex2.Min) + rangex2.Min;
                X1.Add(x1);
                X2.Add(x2);
            }
        }

        //Переопределение функции класса для отрисовки приближенной поверхности
        public override double f(double x, double y)
        {
            /* Тут мы пробегаем по X1,X2 сетки с заданным шагом, но для каждого её узла мы ищем все случайно
             * сгенерированные при создании объекта точки X1,X2 в квадранте вокруг узла.
             * Размер квадранта составляет ровно величину шага по X1 и X2, т.о. обыскивается прямоугольник,
             * углы которого соответствуют центрам смежных узлу квадрантов самой сетки.
             * Как и в случае с «грязной» моделью, для каждой найденной пары X1,X2 добавляется шум.
             * Когда все точки в квадранте (и их значения Y с учетом шума) будут обнаружены, находим среднеарифметическое
             * по формуле сумма(Yi)/количество точек, что будет результирующим Y для данной точки сетки.
             * Здесь важна точность шума, определяющая шаг в расчете нормальных значений для шума методом прямоугольников.
             */
            double valsum = 0;
            int counter = 0;
            Parallel.ForEach(X1, x1 =>
            {
                foreach(double x2 in X2)
                {
                    if (
                        x1 > x - stepx1 / 2 &&
                        x1 < x + stepx1 / 2 &&
                        x2 > y - stepx2 / 2 &&
                        x2 < y + stepx2 / 2
                        )
                    {
                        double noise = fe.normalIntX(rngN.NextDouble(), Sigma, fe.x0, fe.iter);
                        double val = fe.f(x, y) + noise;
                        valsum += val;
                        counter++;
                    }
                }
            });
            if (counter != 0)
            {
                c.counter++;
                c.sumNoise += Math.Abs(fe.f(x, y) - valsum / counter);
            }
            return valsum / counter;
        }
    }

    //В этом классе высчитывается аппроксимационная экспериментальная модель
    public class regressionMapper : nzy3D.Plot3D.Builder.Mapper
    {
        Random masterrng, rng1, rng2, rngN;
        double Sigma = fe.Sigma(fe.rangex1, fe.rangex2);
        double b0 = 0, b1 = 0, b2 = 0, b3 = 0, b4 = 0, b5 = 0;
        Range rangex1 = fe.rangex1;
        Range rangex2 = fe.rangex2;

        double stepx1 = fe.rangex1.Range / fe.steps;
        double stepx2 = fe.rangex2.Range / fe.steps;

        public List<double> X1, X2, Y;

        Multipliers ml = new Multipliers();

        public Multipliers ML() { return ml; }

        Counter c = new Counter();
        public Counter C() { return c; }
        public void C(Counter ps) { c = ps; }

        //n = 2^m = 4
        //na = 2*m = 4
        //nc = 1
        //Итого - 9 опытов
        static int n = 4;
        static int N = 9;

        static double S = Math.Sqrt((double)n / N);
        static double ta = (Math.Sqrt((double)N / n) - 1);
        static double a = Math.Sqrt((double)n / 2 * ta);

        static double[] planx1 = { 1, -1, 1, -1, 0.0, 0.0, -a, a, 0.0 };
        static double[] planx2 = { 1, 1, -1, -1, -a, a, 0.0, 0.0, 0.0 };

        //При создании объекта класса генерируется набор случайных точек X1,X2 в заданном факторном пространстве
        public regressionMapper()
        {
            masterrng = new Random();
            rng1 = new Random(masterrng.Next());
            rng2 = new Random(masterrng.Next());
            rngN = new Random(masterrng.Next());
            int exp = fe.Experiments;
            //Regression regr = new Regression();

            List<double> X1X1, X2X2, X1X2;

            X1 = new List<double>();
            X2 = new List<double>();
            X1X1 = new List<double>();
            X2X2 = new List<double>();
            X1X2 = new List<double>();
            Y = new List<double>();

            simpleParams();

            ml.b0 = Math.Round(b0, 6);
            ml.b1 = Math.Round(b1, 6);
            ml.b2 = Math.Round(b2, 6);
            ml.b3 = Math.Round(b3, 6);
            ml.b4 = Math.Round(b4, 6);
            ml.b5 = Math.Round(b5, 6);
        }

        public regressionMapper(double b0, double b1, double b2, double b3, double b4, double b5)
        {
            masterrng = new Random();
            rngN = new Random(masterrng.Next());

            this.b0 = b0;
            this.b1 = b1;
            this.b2 = b2;
            this.b3 = b3;
            this.b4 = b4;
            this.b5 = b5;

            ml.b0 = Math.Round(b0, 6);
            ml.b1 = Math.Round(b1, 6);
            ml.b2 = Math.Round(b2, 6);
            ml.b3 = Math.Round(b3, 6);
            ml.b4 = Math.Round(b4, 6);
            ml.b5 = Math.Round(b5, 6);
        }

        //Переопределение функции класса для отрисовки приближенной поверхности
        public override double f(double x, double y)
        {
            //Здесь считаем значения Y в точках X1,X2, пробегая по сетке с заданным шагом по заданной регрессионной модели.
            c.counter++;

            double rgrval = b0 + b1 * x + b2 * y + b3 * x * y + b4 * (x * x) + b5 * (y * y);
            double actval = fe.f(x, y)+fe.normalIntX(rngN.NextDouble(), Sigma, fe.x0, fe.iter);

            c.sumNoise += Math.Abs(actval-rgrval);

            return rgrval;
        }

        private void simpleParams()
        {
            /* В этом методе ведется расчет коэффициентов регрессионной модели
             * 
             */
            
            Y = new List<double>();
            int exp = fe.Experiments;
            
            X1.Add(rangex1.Min); X1.Add(rangex1.Max);
            X2.Add(rangex2.Min); X2.Add(rangex2.Max);
            
            /*
            X1.Add(rng1.NextDouble() * (rangex1.Max - rangex1.Min) + rangex1.Min);
            X2.Add(rng2.NextDouble() * (rangex2.Max - rangex2.Min) + rangex2.Min);
            X1.Add(rng1.NextDouble() * (rangex1.Max - rangex1.Min) + rangex1.Min);
            X2.Add(rng2.NextDouble() * (rangex2.Max - rangex2.Min) + rangex2.Min);
            */

            Parallel.For(0, exp, j =>
            {
                List<double> eY = new List<double>();
                List<double> eX1 = new List<double>();
                List<double> eX2 = new List<double>();

                int it = 0;
                do
                {
                    int r = rng1.Next(it, N-1);
                    double temp1 = 0;
                    double temp2 = 0;
                    if (planx1[it] == 1 || planx1[it] == a) temp1 = X1.Max();
                    else if (planx1[it] == -1 || planx1[it] == -a) temp1 = X1.Min();
                    else temp1 = (X1.Min() + X1.Max()) / 2;

                    if (planx2[it] == 1 || planx2[it] == a) temp2 = X2.Max();
                    else if (planx2[it] == -1 || planx2[it] == -a) temp2 = X2.Min();
                    else temp2 = (X2.Min() + X2.Max()) / 2;

                    eX1.Add(planx1[it]);
                    eX2.Add(planx2[it]);

                    eY.Add(fe.f(temp1, temp2) + fe.normalIntX(rngN.NextDouble(), Sigma, fe.x0, fe.iter));

                    it++;
                }
                while (it < N);

                //b0
                double temp = 0;
                foreach (double y in eY)
                {
                    temp += y;
                }
                b0 += temp / N;

                //b1
                temp = 0;
                for (int i = 0; i < N; i++)
                {
                    temp += eX1[i] * eY[i];
                }
                b1 += temp / N;

                //b2
                temp = 0;
                for (int i = 0; i < N; i++)
                {
                    temp += eX2[i] * eY[i];
                }
                b2 += temp / N;

                //b3
                temp = 0;
                for (int i = 0; i < N; i++)
                {
                    temp += eX1[i] * eX2[i] * eY[i];
                }
                b3 += temp / N;

                //b4 (x1^2-S)
                temp = 0;
                for (int i = 0; i < N; i++)
                {
                    temp += (eX1[i] * eX1[i]-S ) * eY[i];
                }
                b4 += temp / N;

                //b5 (x1^2-S)
                temp = 0;
                for (int i = 0; i < N; i++)
                {
                    temp += (eX2[i] * eX2[i]-S) * eY[i];
                }
                b5 += temp / N;
            });

            
            b0 /= exp;
            b1 /= exp;
            b2 /= exp;
            b3 /= exp;
            b4 /= exp;
            b5 /= exp;
            
        }
    }
}

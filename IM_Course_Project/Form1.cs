﻿using System;
using System.Windows.Forms;
using nzy3D.Plot3D.Primitives;
using nzy3D.Plot3D.Primitives.Axes.Layout;
using nzy3D.Plot3D.Rendering.Canvas;
using nzy3D.Chart;

namespace IM_Course_Project
{
    public partial class Form1 : Form
    {
        Model m;
        Shape surface;
        private nzy3D.Chart.Controllers.Thread.Camera.CameraThreadController t;
        private IAxeLayout axeLayout;
        Chart chart;
        double cnr, cns;

        int mode = 1;

        int mock;

        public Form1()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, EventArgs e)
        {
            //Объект класса Model, в котором определены все основные вкусности
            m = new Model();

            //Создаем график и рисуем в нем чистую расчетную поверхность
            chart = new Chart(renderer3D1, Quality.Nicest);

            surface = m.newSurface(new perfectMapper(),checkBox1.Checked);
            checkBox1.Checked = surface.WireframeDisplayed;
            chart.Scene.Graph.Add(surface);

            //Добавляем мышь в качестве устройства ввода для управления камерой
            nzy3D.Chart.Controllers.Mouse.Camera.CameraMouseController mouse = new nzy3D.Chart.Controllers.Mouse.Camera.CameraMouseController();
            mouse.addControllerEventListener(renderer3D1);
            chart.addController(mouse);

            //Создаем выделенный поток для управления камерой мышью
            t = new nzy3D.Chart.Controllers.Thread.Camera.CameraThreadController();
            t.addControllerEventListener(renderer3D1);
            mouse.addSlaveThreadController(t);
            chart.addController(t);
            t.Start();

            //Привязываем график к рендереру в окне
            renderer3D1.setView(chart.View);
        }

        //Тут при закрытии формы выбрасываем поток управления камерой
        private void Window_Closing(object sender, FormClosingEventArgs e)
        {
            DisposeBackgroundThread();
        }

        private void DisposeBackgroundThread()
        {
            if ((t != null))
            {
                t.Dispose();
            }
        }

        //Рисуем чистую поверхность
        private void button1_Click(object sender, EventArgs e)
        {
            chart.Scene.Graph.Remove(surface);
            surface = m.PerfectSurface();
            surface.WireframeDisplayed = checkBox1.Checked;
            chart.Scene.Graph.Add(surface);
            label2.Text = "0.0";
            mode = 1;
        }

        //Рисуем грязную поверхность
        private void button2_Click(object sender, EventArgs e)
        {
            chart.Scene.Graph.Remove(surface);
            surface = m.NoisySurface();
            surface.WireframeDisplayed = checkBox1.Checked;
            chart.Scene.Graph.Add(surface);
            label2.Text = m.Cns().ToString();
            mode = 2;
        }

        //Рисуем аппроксимационную поверхность
        private void button3_Click(object sender, EventArgs e)
        {
            chart.Scene.Graph.Remove(surface);
            surface = m.NormalSurface();
            surface.WireframeDisplayed = checkBox1.Checked;
            chart.Scene.Graph.Add(surface);
            label2.Text = m.Cnm().ToString();
            mode = 3;
        }

        //Рисуем регрессионную модель
        private void button5_Click(object sender, EventArgs e)
        {
            chart.Scene.Graph.Remove(surface);
            surface = m.RegressionSurface();
            surface.WireframeDisplayed = checkBox1.Checked;
            chart.Scene.Graph.Add(surface);
            label2.Text = m.Crg().ToString();
            toolStripStatusLabel1.Text = status(m.b0, m.b1, m.b2, m.b3, m.b4, m.b5);
            mode = 4;
        }

        //Включаем или выключаем отрисовку сетки
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            surface.WireframeDisplayed = checkBox1.Checked;
        }

        //Рисуем по заданным коэффициентам
        private void button6_Click(object sender, EventArgs e)
        {
            double b0, b1, b2, b3, b4, b5;
            if (Double.TryParse(textBox4.Text, out b0) && Double.TryParse(textBox5.Text, out b1) && Double.TryParse(textBox6.Text, out b2) &&
                Double.TryParse(textBox7.Text, out b3) && Double.TryParse(textBox8.Text, out b4) && Double.TryParse(textBox9.Text, out b5))
            {
                chart.Scene.Graph.Remove(surface);
                surface = m.newSurface(new regressionMapper(b0, b1, b2, b3, b4, b5), true);
                label2.Text = m.Crg().ToString(); toolStripStatusLabel1.Text = status(b0, b1, b2, b3, b4, b5);
                chart.Scene.Graph.Add(surface);
            }
            else
            {
                MessageBox.Show("Check your input.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }


        //Генерируем полностью новый набор поверхностей-моделей
        private void button4_Click(object sender, EventArgs e)
        {
            if (Int32.TryParse(textBox1.Text, out mock) && Int32.TryParse(textBox2.Text, out mock) && Int32.TryParse(textBox3.Text, out mock))
            {
                m.generateNewSurfaces(Int32.Parse(textBox1.Text), Int32.Parse(textBox2.Text), Int32.Parse(textBox3.Text), checkBox1.Checked);
                chart.Scene.Graph.Remove(surface);
                switch (mode) {
                    case (1): { surface = m.PerfectSurface(); label2.Text = "0.0"; break; }
                    case (2): { surface = m.NoisySurface(); label2.Text = m.Cns().ToString(); break; }
                    case (3): { surface = m.NormalSurface(); label2.Text = m.Cnm().ToString(); break; }
                    case (4): { surface = m.RegressionSurface(); label2.Text = m.Crg().ToString(); toolStripStatusLabel1.Text = status(m.b0,m.b1,m.b2,m.b3,m.b4,m.b5); break; }
                }
                chart.Scene.Graph.Add(surface);
            }
            else
            {
                MessageBox.Show("Check your input.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private String status(double b0,double b1, double b2, double b3, double b4, double b5)
        {
            return "y = (" + b0.ToString() + ") + (" + b1.ToString() + ")x1 + (" + b2.ToString() + ")x2 + (" + b3.ToString() + ")x1x2 + (" + b4.ToString() + ")x1^2 + (" + b5.ToString() + ")x2^2";
        }
    }
}
